run();
function setUpProjects() {
    let projects = [];
    projects.push(
        new Project('Bomberman js',
            'Un petit jeu en js natif réalisé à Simplon. Le système de génération de carte aléatoire est assez rigolo !',
            'https://gitlab.com/samuelkeller/bomberman/raw/master/img/1.png',
            ['js', 'Natif', 'Simplon'],
            '#',
            'https://gitlab.com/samuelkeller/bomberman'
        ),
        new Project('Calendar Challenge',
            'Pendant une semaine, réaliser un calendrier en JS en utilisant la librairie momentjs.',
            'images/calendar-challenge.png',
            ['moment.js', 'js', 'boostrap'],
            '#',
            'https://gitlab.com/jeanjacques42/calendar-challenge?nav_source=navbar'
        ),
        new Project('Simplon Gear',
            'Un site eCommerce réalisé en trois semaines sur Symphony avec gestion d\'utilisateurs, de panier et produits avec caracteristiques',
            'images/simplonGear.png',
            ['Symphony 4', 'Twig'],
            '#',
            'https://gitlab.com/samuelkeller/simplon-gear?nav_source=navbar'
        ),
        new Project('GeoGame',
        'Un jeu réalisé en deux jours sur l\'API RESTcountries pour ce familiariser avec le REST et axios',
        'images/GeoGame.png',
        ['RESTCountries', 'Axios', 'REST'],
        '#',
        'https://gitlab.com/samuelkeller/geogame'
        ),
        new Project('PDO Blog',
        'Un blog réalisé seulement avec PDO pour apprendre la gestion de base de données. Le design repose principalement sur Boostrap mais est assez chouette.',
        ['images/blog1.png', 'images/blog2.png'],
        ['PDO', 'Boostrap 4'],
        '#',
        'https://gitlab.com/samuelkeller/projet-blog-pdo'
        )
    )
    return projects;
}
function run() {
    console.log('hello');
    
    let projects = setUpProjects();
    for (let i = 0; i < projects.length; i++) {
        projects[i].displayProject("#article" + i);
        projects[i].addModal("article" + i);
        
    }
}
class Project {
    /**
     * 
     * @param {string} title Project Title
     * @param {string} description Project description
     * @param {path} image Project image
     * @param {array} tags Project tags in an array
     * @param {string} link Link to visible project
     * @param {string} repo Link to gitlab repo
     */
    constructor(title, description, image, tags, link, repo) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.tags = tags.join(' | ');
        this.link = link;
        this.repo = repo;
    }
    addModal(newClass){
        let parent = document.querySelector('.gallery-happend');

        let modal = document.createElement('div');
        let modalDialog = document.createElement('div');
        let modalContent = document.createElement('div');
        let modalHeader = document.createElement('div');
        let modalTitle = document.createElement('h5');
        let modalBody = document.createElement('div');
        let closeButton = document.createElement('button');

        let col = document.createElement('div');
        let col2 = document.createElement('div');
        let description = document.createElement('p');
        let link1 = document.createElement('a');
        let link2 = document.createElement('a');
        let linkdiv = document.createElement('div');

        link1.setAttribute('href', this.link)
        link1.innerHTML = '<i class="fas fa-4x fa-link"></i>';

        link2.setAttribute('href', this.repo)
        link2.innerHTML = '<i class="fab fa-4x fa-gitlab"></i>';

        modal.className = 'modal fade';
        modalDialog.className = 'modal-dialog';
        modalContent.className = 'modal-content';
        modalBody.className = 'modal-body row';

        // link1.innerHTML=

        col.className = 'col-6';
        col2.className = 'col-6';
        description.textContent = this.description;

        
        if(this.image.constructor === Array){
            col.style.backgroundImage ='url("'+ this.image[1] + '")';
        }else{
            col.style.backgroundImage ='url("'+ this.image + '")';
        }



        modalHeader.className = 'modal-header';
        modalTitle.textContent = this.title;
        closeButton.className = 'btn btn-outline-danger';
        closeButton.innerHTML = '<i class="far fa-times-circle"></i>';

        modal.style.display = 'none';
        closeButton.setAttribute('data-dismiss', 'modal')

        parent.appendChild(modal);
        modal.appendChild(modalDialog);
        modal.id = newClass;
        modalDialog.appendChild(modalContent);
        modalContent.appendChild(modalHeader);
        modalHeader.appendChild(modalTitle);
        modalHeader.appendChild(closeButton);
        modalContent.appendChild(modalBody);
        modalBody.appendChild(col);
        modalBody.appendChild(col2);

        col2.appendChild(description)
        col2.appendChild(linkdiv);
        linkdiv.appendChild(link1);
        linkdiv.appendChild(link2);
    }
    displayProject(newClass) {
        let parent = document.querySelector('.gallery-happend');

        let btn = document.createElement('button');
        let article = document.createElement('article');
        let div = document.createElement('div');
        let img = document.createElement('img');
        let para = document.createElement('p');
        let tags = document.createElement('p');

        article.className = 'col-lg-3 col-sm-6';
        if(this.image.constructor === Array){
            img.src = this.image[0];
        }else{
            img.src = this.image;
        }
        para.textContent = this.description;
        tags.textContent = this.tags;
        tags.className = 'light';
        btn.setAttribute('data-toggle', 'modal');
        btn.setAttribute('data-target', newClass);
        btn.type = 'button';
        btn.className = 'btn btn-outline-warning';


        btn.textContent = 'More';

        parent.appendChild(article);
        article.appendChild(div);
        div.appendChild(img);
        div.appendChild(para);
        div.appendChild(tags);
        div.appendChild(btn);
    }
}
